# -----------------------------------------------------------------------------
# Name:        grupo01.py
# Purpose:     Trabajo Practico
#
# Author:      Martin Sandroni
#              Lucas Zurverra
#
# Created:     18/06/2013
# Copyright:   (c) Sandroni-Zurverra 2013
# -----------------------------------------------------------------------------
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------

def obtener_simbolos(cadena, separador):
    ''' Devuelve una lista con elementos de una cadena (elimina espacios) '''
    lista = cadena.split(separador)

    return lista

def setear_gramatica(ruta):
    ''' Especificar cual es la gramatica de entrada. Devuelve un objeto que al
    pasarlo a <string> tiene la representacion de la gramatica, donde en cada
    regla hay 3 listas a su derecha: Firsts, Follows y Selects correpondientes.
    Ademas, este objeto, posee un campo <EsLL1> de tipo <Booleano> que
    indica si la gramatica puede reconocerse o no mediante esta tecnica. '''

    # Lee el archivo que contiene la gramatica y elimina los saltos de linea
    f = (l.rstrip('\r\n') for l in file("gramatica.txt", 'r'))

    # Anexa cada regla a la lista
    global reglas
    reglas = []
    for lineas in f:
        reglas.append(lineas)

    '''
    Contiene toda la gramatica, almacenada en una lista.
    Cada elemento es otra lista con antecedente y consecuente
    Ej: [[A,C],[A,C],...]
    '''
    global no_terminales
    no_terminales = []

    # Lista que contendra una lista con el antecedente seguido del consecuente
    for linea in range(len(reglas)):
        # Divide la regla en antecedente [0] y consecuente [1]
        lista_aux = obtener_simbolos(reglas[linea], ' : ')
        # Guarda la lista en otra lista. Ej: [[A,C],[A,C]]
        reglas.append(lista_aux)
        # En cada iteracion va almacenando el antecedente
        antecedente = reglas[linea][0]

        # Si el antecedente es un Terminal, lo guarda en una lista
        if antecedente not in no_terminales:
            no_terminales.append(antecedente)

        # Contiene los No-Terminales sin repetir
        no_terminales = list(set(reglas))

    # Obtener lista de Firsts, Follows y Selects-------------------------------
    fi = firsts(no_terminales, reglas)
    fo = follows(no_terminales, reglas)
    se = selects(fi, fo, reglas)

    # Llama a la funcion para comprobar si es LL1
    EsLL1 = comprobar_ll1(se, reglas)

    return reglas, EsLL1, no_terminales


def firsts(no_terminales, reglas):
    '''
    Recorre gramatica calculando primero firsts de reglas que arrancan con
un Terminal.
Luego hace los calculos para obtener los de las reglas que empiezan con
un No-Terminal
    '''
    ubica = -1
    posicion = -1
    V = 0
    # Listar Terminales que son primer consecuente
    lista_firsts = [[]] # Lista que almacena los firsts
    # Guardamos las reglas que arrancan con NT para luego calcular sus firsts
    lista_nt = []
    pos = -1
    B = 0
    c = -1    # Contador
    for linea in reglas:
        c += 1
        consecuente = obtener_simbolos(reglas[c][1])
        primero = consecuente[0]    # Primer consecuente
        if primero not in no_terminales:    # El primero es un terminal
            pos += 1
            # Guardo la referencia a la regla y su first
            lista_firsts.append([pos, primero])
        else:   # Es un no terminal
            lista_nt.append(reglas[c])   # Guardo en la lista la regla entera
    # Listar antecedentes que tienen No-Terminales como primer elemento
    l = -1
    for linea in lista_nt:
        l += 1
        # Guardamos la regla actual para luego guardar los firsts
        regla = lista_nt[l]
        pos = -1
        while B == 0:   # Condicion de corte
            for ubicacion in reglas: # Buscamos la regla para poder obtener su posición en la gramatica
                pos += 1
                # La regla de lista_nt es igual a la regla en "reglas"
                if regla == reglas[pos]:
                    posicion = pos    # Guardo la posicion de la regla
                    B = 1    # Genero el corte
                    # Guardo consecuentes de la regla sacada de lista_nt
            consecuente_lista_nt = obtener_simbolos(lista_nt[l], ' ')
            t = 0    # Puntero para la lista de consecuentes
            #Guardo el primer elemento del consecuente
            simbolo = consecuente_lista_nt[t]
            # Por cada consecuente recorro mientras sea "V" igual a cero
            while V == 0:
                c = -1
                for linea in reglas:
                    c += 1
                    # Guardo antecedente de la regla
                    antecedente = reglas[c][0]
                    # Si simbolo(consecuente) es igual a antecedente
                    if simbolo == antecedente:
                        if c in lista_firsts:   # Ya tiene firsts cargados
                            if 'lambda' not in lista_firsts[c][1]:  # Lambda no está en el first
                                # Agrego los firsts
                                lista_firsts[c].append(lista_firsts[c][1])
                            else:   # Lambda está en el first
                                if not consecuente_lista_nt[t] == consecuente_lista_nt[-1]:     # Si el elemento actual no es el ultimo
                                    if consecuente_lista_nt[t+1] not in no_terminales:      # Siguiente elemento es terminal
                                        lista_firsts[c].append(lista_firsts[c][1])  # Agrego los firsts
                                    else:       # El siguiente es un no terminal
                                        r = -1
                                    for recorre in consecuente_lista_nt:
                                        r+=1
                                        if r > 1:       # Arrancamos con el segundo elemento del consecuente
                                            aux = consecuente_lista_nt[r]       # Guardo el consecuente
                                            u=-1
                                            for rule in reglas: # Busco el consecuente en reglas
                                                u+=1
                                                if aux == reglas[u][0]: #Consecuente igual antecedente
                                                    ubica = u    # Guardo numero de regla
                                                lista_firsts[c].append(lista_firsts[c][1])      #Guardo los firsts
                                                if 'lamda' in lista_firsts[u][1]:       # Si lambda esta en los firsts
                                                    b=1
                                                    break    # Terminé de calcular los firsts del elemento
                                    if b == 1:
                                        break
                        else:   # No tiene firsts cargados
                            if 'lambda' not in lista_firsts[c][1]:  # Lambda no está en el first
                                lista_firsts.append([c,lista_firsts[c][1]]) # Agrego los firsts
                            else:   # Lambda esta en el first
                                if consecuente_lista_nt[t+1] not in no_terminales or consecuente_lista_nt[t] == consecuente_lista_nt[-1]:      # Siguiente elemento es terminal o el actual es el último
                                    lista_firsts.append[c,(lista_firsts[c][1])] # Agrego los firsts
                                else:   # Lambda no esta en el first
                                    if not consecuente_lista_nt[t] == consecuente_lista_nt[-1]:     # Si el elemento actual no es el ultimo
                                        if consecuente_lista_nt[t+1] not in no_terminales:      # Siguiente elemento es terminal
                                            lista_firsts[c].append([c,lista_firsts[c][1]])  # Agrego los firsts
                                        else:       # El siguiente es un no terminal
                                            r = -1
                                        for recorre in consecuente_lista_nt:
                                            r+=1
                                            if r > 1:       # Arrancamos con el segundo elemento del consecuente
                                                aux = consecuente_lista_nt[r]       # Guardo el consecuente
                                                u=-1
                                                for rule in reglas: # Busco el consecuente en reglas
                                                    u += 1
                                                    if aux == reglas[u][0]: #Consecuente igual antecedente
                                                        ubica = u    # Guardo numero de regla
                                                    lista_firsts[c].append([c,lista_firsts[c][1]])     #Guardo los firsts
                                                    if 'lamda' in lista_firsts[u][1]:       # Si lambda esta en los firsts
                                                        b=1
                                                        break    # Terminé de calcular los firsts del elemento
                                            if b == 1:
                                                break
                    if b == 1:
                        break

    lista_firsts.sort()
    anexar_lista(lista_firsts,reglas)

    return lista_firsts


def follows(no_terminales, reglas):
    '''
    Copio lista no_terminales.
    Recorro reglas y comparo, así calculo follows.
    Guardo follows en lista.
    '''
    lista_follows=[[]]
    lista_firsts = []
    aux=no_terminales	# Copio no terminales en aux
    reinicio=True
    while reinicio:
        reinicio = False
	for a in aux:
	    band=0	# Bandera
	    h=0    # Puntero de aux
	    i=-1
	    for linea in reglas:    # Puntero para ir agarrando las reglas
	        i+=1
		if h == 0:	# Si es la primer regla
		    lista_follows[0].append(h)  # Guardo la posición
                    lista_follows[0].extend('$')    # Guardo $ a la primera regla
		else:	# Si no
		    lista_follows[i-1].append([i])	# Guardo la referencia de la regla
		    consecuente = obtener_simbolos(reglas[i][1],' ')	# Saco consecuente regla
		    c=-1
		    for cons in consecuente:	# Recorro consecuente
		        c+=1
			if aux[h] == consecuente[c]:	# Si NT está en el consecuente
			    if consecuente[c] == consecuente[-1]:	# Si es último elemento
			        if i in lista_follows:	# Si los follows del antecedente están calculados
				    s=-1
				    for seek in reglas:	# Busco en reglas
				        s+=1									
					if aux[h] == reglas[s][0]:	# Si NT es igual antecedente
					    posicion = s	# Guardo posicion de regla
					    r=-1
					    for recorre in lista_follows:	# Recorro lista follows
					        r+=1
						if posicion == lista_follows[r][0]:	# Si encuentro posicion regla
                                                    lista_follows[r].extend([posicion,lista_follows[i][1]])	# Guardo follows del antecedentes
						else:	# Si los follows del antecedente no están calculados
						    band=1
						    break	# Corto para calcular follows proximo NT
			    else:	# Si no es último elemento
			        if consecuente[c+1] not in no_terminales:	# Es un terminal
				    lista_follows.append([h,consecuente[c+1]])	# Guardo el terminal como un follow	
				else:	# Es un no terminal
				    lista_follows.append([h,lista_firsts[i][1]])	# Guardo los firsts de la regla como follows
		    if band == 1:
		        break	# Corto para calcular follows proximo NT
        aux.pop(h)	# Saco el NT ya que obtuve todos sus follows
    if not aux == []:	# Si faltan follows por calcuar
        reinicio = True   # Reinicia el for para calcular follows de reglas restantes
    else:	#Si están todos los follows calculados
	reinicio = False	# Termina

    lista_follows.sort()
    anexar_lista(lista_follows,reglas)
	
    return lista_follows


def selects(lista_fi, lista_fo, reglas):
    ''' Por cada linea, hace una lista de selects y almacena la lista_fi.
    Despues, comprueba si lista_selects tiene <lambda> y lo reemplaza
    por los valores de la lista_fo '''

    ''' Comprobar si en la lista de Firsts existe un lambda y reemplazarlo
por los Follows de esa regla '''
    for linea in range(len(lista_fi)):
        # El conjunto Select tiene lambda
        simbolos = obtener_simbolos(lista_fi, ' ')
        if 'lambda' in simbolos:
            # Lista de Selects que contendrian los First
            lista_se = obtener_simbolos(lista_fi[linea], ' ')
            # Guarda la posicion en donde se encuentra lambda
            indice = lista_se.index('lambda')
            # Coloca los Follows donde se encontraba lambda
            lista_se[indice].extend(lista_fo)

    #La lista reglas contendra [[A,C,[Fi],[Fo],[Se]],[...]]
    for linea in range(len(reglas)):
        # Guarda los Selects en cada regla de la lista reglas
        anexar_lista(lista_se)

        return lista_se


def comprobar_ll1(lista_se, reglas):
    ''' Guarda indices de reglas con igual antecedente y compara
sus Selects. Devuelve True en caso de ser distintos en todas las reglas.
De lo contrario, devuelve False '''
    indices = []        # Lista de indices donde se repite el antecedente
    selects_aux = []      # Selects de reglas con igual antecedente
    for linea in range(len(reglas)):
        antecedente_lista = obtener_simbolos(reglas[linea][0])
        antecedente = antecedente_lista[0]
        # Las reglas con el mismo antecedente las almacena para compararlas
        if antecedente in reglas[linea][0]:
            indices = reglas[linea].index()
        # Guarda los Selects de las reglas con igual antecedente
        if len(indices) > 1:
            for i in range(len(reglas)):
                selects_aux.append(reglas[i][4])
        else:    # Continua comparando
            continue
        # Compara los Selects de las reglas que aparecen mas de una vez
        for i in range(len(selects_aux)):
            s = selects_aux.pop()
            simbolos = obtener_simbolos(s, ' ')
            for x in range(len(simbolos)):
                if simbolos in s:
                    comprobacion = False
                    break
                else:
                    comprobacion = True

    return comprobacion


def anexar_lista(lista, reglas):
    ''' Anexa una lista dada en la lista reglas '''
    for linea in range(len(lista)):
        reglas[linea][1].append(lista)


def evaluar_cadena(cadena, no_terminales, reglas):
    '''
    Devuelve:
             <True> en caso de que la cadena se derive de la gramatica
    previamente seteada.
             <False> en caso contrario.
    '''
    pila = []
    simbolos = obtener_simbolos(cadena,' ') #Obtengo los elementos de la cadena separados
    consecuente = obtener_simbolos(reglas[0][1],' ')    #Tengo los elementos del consecuente de la primer regla
    pila.append(consecuente)    #Agrego a pila esos consecuentes
    while pila != []:
        if pila[-1] in no_terminales:   #Si el ultimo elemento de simbolos es un NT
            pos=-1    #Contador de posicion
            for linea in reglas:
                pos=pos+1
                if pila[-1] == reglas[pos][1]: #Compara para encontrar la posicion
                    pila.pop(-1)  #Saca el último elemento de la pila
                    ultimo = obtener_simbolos(reglas[pos][1],' ')   #Guarda los consecuentes separados
                    pila.append(ultimo) #Añade los consecuentes a la pila
                else:   #Si no es un NT
                    if pila[-1] in simbolos:     #Si está en la gramatica
                        p=-1
                        for elemento in simbolos:
                            p=p+1
                            if pila[-1] == simbolos[p]: #Busco la posicion
                                simbolos.pop(p) #Saco el elemento de simbolos
                        pila.pop(-1)    #Saca el último elemento de la regla
                    else:   #Si no está en la gramatica
                        return False
        if pila != []:
            return True


def main():
    # Obtiene una lista con las reglas de la gramatica
    g = setear_gramatica('gramatica.txt')

    # Comprueba si la gramatica es LL1
    if g[1] is True:
        msj = 'La gramatica es LL1'
    else:
        msj = 'La gramatica no es LL1'

    # Evaluacion de cadenas
    e = evaluar_cadena(raw_input('> Ingrese la cadena a evaluar: '), g[2], g[0])
    if e is True:
        msj2 = 'La cadena: %s PERTENECE al lenguaje\n' % e
    else:
        msj2 = 'La cadena: %s NO pertenece al lenguaje\n' % e

    # Imprimir los resultados
    print 'La gramatica de entrada es:\n %s\n', g[0]
    print '\n'
    print msj
    print msj2

    # Archivo de salida
    f = open('output.txt', 'w')
    f.close()


    # Coloca una pausa para ver los resultados en pantalla
    raw_input('Presione una tecla para finalizar...')

if __name__ == '__main__':
    main()
