Trabajo Práctico
==========

Se debe escribir un programa que genere un parser LL (1) a partir de una gramática dada para luego poder determinar si una cadena se deriva de la misma.
Para cumplir con este objetivo debe programar un módulo en Python con las siguientes características:
1) Poseer una función setear_gramatica(string) para poder especificar cual es la gramática de entrada. Esta función debe devolver un objeto que al pasarlo a string tenga la representación de la gramática, donde cada regla posea 3 listas a su derecha, los Firsts, Follows y Selects correpondientes. Además, este objeto, debe
poseer un campo EsLL1 de tipo Booleano, indicando si la gramática puede
reconocerse o no mediante esta técnica.
2) Poseer una función evaluar_cadena(string), la cual devuelve True, en caso de que la
cadena se derive de la gramática previamente seteada y False en caso contrario.
El módulo programado debe llamarse grupoxx.py donde xx es el número de grupo
correspondiente.

Consideraciones:
+ Se considera no terminal a cualquier palabra que comienza con letra mayúscula.
+ Terminales y No-Terminales pueden contener más de una letra
+ El símbolo “:” y la palabra “lambda” son reservadas.
+ La antecedente de la primera regla de la gramática es el no terminal distinguido.
+ Las producciones se separan con \n, el antecedente del consecuente con “:” y los
elementos del consecuente con espacio/s.


Ejemplo de entrada
-----------------------------------
A:bA
A:a
A:ABc
A : lambda
B:b


Ejemplo de salida
-----------------------------------
A : b A [b] [$, b] [b]
A : a [a] [$, b] [a]
A : A B c [b] [$, b] [b]
A : lambda [lambda] [$, b] [$, b]
B : b [b] [c] [b]      
